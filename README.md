# Wigner-hooke

This code can be used to numerically solve the two-particle problem in an anisotropic confinement potential (anisotropic Hooke's atom).

It follows the reasoning and notation from: https://link.aps.org/doi/10.1103/PhysRevB.104.195305
If you found this code useful for your work, please cite it!

This code is made to play around with the anisotropic two-electron/hole problem. You'll be able to get the renormalization of the ST splitting as a function of anisotropy and interaction strength.

## Usage
Open the notebook Wigner-stfun.nb with Mathematica and... have fun!

## Support
For any doubts ask jose.garciaabadillouriel@cea.fr

